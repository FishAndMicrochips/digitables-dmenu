/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Source Code Pro:style=regular:pixelsize:10",
	"FontAwesome:pixelsize=10:antialiasing=false"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

static const char col_black[] = "#131313";
static const char col_red  [] = "#cc6666";
static const char col_white[] = "#eeeeee";
static const char col_gray [] = "#bbbbbb";
static const char col_gold [] = "#f0c674";

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm]          = { col_gray, col_black },
	[SchemeSel]           = { col_white, col_red },
	[SchemeSelHighlight]  = { col_gold,  col_red },
	[SchemeNormHighlight] = { col_white, col_black },
	[SchemeOut]           = { "#000000", "#ff00ff" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
static unsigned int lineheight = 0;         /* -h option; minimum height of a menu line     */

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
